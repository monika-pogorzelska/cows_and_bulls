class ScoreCounter
  attr_reader :number, :user_number, :cows, :bulls

  def initialize(number, user_number)
    @number = number
    @user_number = user_number
    @cows = number_of_cows
    @bulls = number_of_bulls
  end

  private

  def number_of_cows
    cows = 0
    number.each_with_index do |value, index|
      if value == user_number[index]
        cows += 1
      end
    end
    cows
  end

  def number_of_bulls
    bulls = user_number.select { |i| number.include?(i) }
    bulls.count - @cows
  end
end
