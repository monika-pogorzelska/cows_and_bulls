require 'spec_helper'
require_relative '../lib/score_counter.rb'

describe ScoreCounter do
  context 'cows and bulls' do
    let(:number) { [1,2,3,4] }
    let(:user_number) { [1,2,4,6] }
    subject { described_class.new(number, user_number) }

    it 'should return score' do
      expect(subject.cows).to eq(2)
      expect(subject.bulls).to eq(1)
    end
  end

  context '0 cows and some bulls' do
    let(:number) { [2,2,3,4] }
    let(:user_number) { [1,1,2,6] }
    subject { described_class.new(number, user_number) }

    it 'should return score' do
      expect(subject.cows).to eq(0)
      expect(subject.bulls).to eq(1)
    end
  end

  context '0 cows and some bulls' do
    let(:number) { [2,6,3,4] }
    let(:user_number) { [3,1,9,3] }
    subject { described_class.new(number, user_number) }

    it 'should return score' do
      expect(subject.cows).to eq(0)
      expect(subject.bulls).to eq(2)
    end
  end
end
