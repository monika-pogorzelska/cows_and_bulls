require_relative 'lib/score_counter'

def player_interaction
  puts "Please enter 4 digit number"
  input = STDIN.gets.chomp
  input.length == 4 ? input : player_interaction
end

random_number = rand.to_s[2..5]

while player = player_interaction
  computer_number = random_number.chars.map(&:to_i)
  user_number = player.chars.map(&:to_i)
  score = ScoreCounter.new(computer_number, user_number)
 
  if score.cows == 4
    puts '4 cows, congratulations!'
    exit
  else
    puts "#{score.cows} cows, #{score.bulls} bulls"
  end
end
